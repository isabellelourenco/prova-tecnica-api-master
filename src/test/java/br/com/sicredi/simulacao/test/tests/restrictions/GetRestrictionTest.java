package br.com.sicredi.simulacao.test.tests.restrictions;

import br.com.sicredi.simulacao.test.factories.restriction.RestrictionDataFactory;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.hamcrest.Matchers.equalTo;

public class GetRestrictionTest extends RestrictionBaseTest{

    @Test
    @DisplayName("When request cpf without restriction, then should return no content status")
    public void whenRequestCpfWithoutRestriction_ShouldReturnRestrictionMessage(){

        String cpfWithoutRestriction = RestrictionDataFactory.cpfWithoutRestriction();

        requestRestriction(cpfWithoutRestriction)
                .statusCode(HttpStatus.SC_NO_CONTENT);

    }

    @Test
    @DisplayName("When request cpf with restriction, then should return a cpf restriction message")
    public void whenRequestCpfWitRestriction_ShouldReturnRestrictionMessage(){

        String cpfWithRestriction = RestrictionDataFactory.cpfWithRestriction();

        requestRestriction(cpfWithRestriction)
                .statusCode(HttpStatus.SC_OK)
                .body("mensagem", equalTo("O CPF " + cpfWithRestriction + " possui restrição"));

    }

}
