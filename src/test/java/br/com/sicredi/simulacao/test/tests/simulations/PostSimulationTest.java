package br.com.sicredi.simulacao.test.tests.simulations;

import br.com.sicredi.simulacao.test.factories.simulation.SimulationDataFactory;
import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.equalTo;

public class PostSimulationTest extends SimulationBaseTest {

    SimulationPojo simulationToRegister = new SimulationPojo();
    Integer simulationIdToDelete;

    @Test
    @DisplayName("When create a valid new simulation, then should be created with success")
    public void whenCreateValidNewSimulation_ShouldBeCreatedWithSuccess() {

        simulationToRegister = SimulationDataFactory.buildValidSimulation();

        simulationIdToDelete = registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_CREATED)
                .body("nome", equalTo(simulationToRegister.getNome()))
                .body("cpf", equalTo(simulationToRegister.getCpf()))
                .body("email", equalTo(simulationToRegister.getEmail()))
                .body("valor", equalTo(simulationToRegister.getValor()))
                .body("parcelas", equalTo(simulationToRegister.getParcelas()))
                .body("seguro", equalTo(simulationToRegister.getSeguro()))
                    .extract().path("id");

        deleteSimulation(Long.valueOf(simulationIdToDelete));

    }

    @Test
    @DisplayName("When record a invalid simulation with empty attributes, then should return a empty errors list")
    public void whenRecordInvalidSimulationWithEmptyAttributes_ShouldReturnError() {

        simulationToRegister = SimulationDataFactory.buildEmptySimulation();

        registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.parcelas", equalTo("Parcelas não pode ser vazio"))
                .body("erros.cpf", equalTo("CPF não pode ser vazio"))
                .body("erros.valor", equalTo("Valor não pode ser vazio"))
                .body("erros.nome", equalTo("Nome não pode ser vazio"))
                .body("erros.email", equalTo("E-mail não deve ser vazio"))
                .body("erros.seguro", equalTo("Uma das opções de Seguro devem ser selecionadas"));

    }

    @Test
    @DisplayName("When record a simulation with duplicated CPF, then should return conflict error")
    public void whenRecordSimulationWithDuplicatedCpf_ShouldReturnConflictError(){

        simulationToRegister = SimulationDataFactory.buildValidSimulation();
        registerSimulation(simulationToRegister);

        registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_CONFLICT)
                .body("mensagem",equalTo("CPF já existente"));
    }

    @Test
    @DisplayName("When record a simulation with less than min amount, then should return bad request error")
    public void whenRecordSimulationWithLessThanMinAmount_ShouldReturnBadRequestError(){

        simulationToRegister = SimulationDataFactory.buildSimulationLessThanMinAmount();

        registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.valor",equalTo("Valor deve ser igual ou maior que R$ 1.000"));
    }

    @Test
    @DisplayName("When record a simulation with exceed max amount, then should return bad request error")
    public void whenRecordSimulationExceedMaxAmount_ShouldReturnBadRequestError(){

        simulationToRegister = SimulationDataFactory.buildSimulationExceedMaxAmount();

        registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.valor",equalTo("Valor deve ser menor ou igual a R$ 40.000"));
    }

    @Test
    @DisplayName("When record a simulation with less than min installments, then should return bad request error")
    public void whenRecordSimulationWithLessThanMinInstallments_ShouldReturnBadRequestError(){

        simulationToRegister = SimulationDataFactory.buildSimulationLessThanMinInstallments();

        registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.parcelas",equalTo("Parcelas deve ser igual ou maior que 2"));
    }

    @Test
    @DisplayName("When record a simulation with exceed max installments, then should return bad request error")
    public void whenRecordSimulationExceedMaxInstallments_ShouldReturnBadRequestError(){

        simulationToRegister = SimulationDataFactory.buildSimulationExceedInstallments();

        registerSimulation(simulationToRegister)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.parcelas",equalTo("Parcelas deve ser menor ou igual a 48"));
    }

}
