package br.com.sicredi.simulacao.test.tests.simulations;

import br.com.sicredi.simulacao.test.factories.simulation.SimulationDataFactory;
import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.equalTo;

public class PutSimulationTest extends SimulationBaseTest {


    @Test
    @DisplayName("When update an existing simulation, then simulation should be updated with success")
    public void whenUpdateSimulation_ShouldSimulationUpdatedWithSuccess(){

        simulationToUpdate = SimulationDataFactory.buildValidSimulation();
        String cpfToUpdateSimulation = getCpfFromNewSimulation(simulationToUpdate);
        SimulationPojo simulationUpdated = updateSimulationData(simulationToUpdate);

        Integer simulationIdToDelete = updateSimulation(cpfToUpdateSimulation, simulationToUpdate)
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("nome", equalTo(simulationUpdated.getNome()))
                .body("cpf", equalTo(simulationUpdated.getCpf()))
                .body("email", equalTo(simulationUpdated.getEmail()))
                .body("valor", equalTo(simulationUpdated.getValor()))
                .body("parcelas", equalTo(simulationUpdated.getParcelas()))
                .body("seguro", equalTo(simulationUpdated.getSeguro()))
                .extract().path("id");

        deleteSimulation(Long.valueOf(simulationIdToDelete));

    }

    @Test
    @DisplayName("When update for non-existing CPF simulation, then should return not found error")
    public void whenUpdateNotExistingCpf_ShouldReturnNotFoundError(){

        simulationToUpdate = SimulationDataFactory.buildValidSimulation();
        String notExistentCpf = SimulationDataFactory.notExistentCpf();

        updateSimulation(notExistentCpf,simulationToUpdate)
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem",equalTo("CPF não encontrado"));

    }

    SimulationPojo simulationToUpdate = new SimulationPojo();

}
