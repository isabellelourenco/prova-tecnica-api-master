package br.com.sicredi.simulacao.test.factories.simulation;

import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import com.github.javafaker.Faker;

import java.math.BigDecimal;

public class SimulationDataFactory {

    public static SimulationPojo buildValidSimulation(){

        Faker faker = new Faker();

        SimulationPojo validSimulation = new SimulationPojo();
        String cpf = Long.toString(faker.number().randomNumber(11,true));

        validSimulation.setNome(faker.name().firstName());
        validSimulation.setCpf(cpf);
        validSimulation.setEmail(faker.internet().emailAddress());
        validSimulation.setValor(BigDecimal.valueOf(faker.number().randomDouble(2,MIN_AMOUNT,MAX_AMOUNT)));
        validSimulation.setParcelas(faker.number().numberBetween(MIN_INSTALLMENTS,MAX_INSTALLMENTS));
        validSimulation.setSeguro(faker.bool().bool());

        return validSimulation;
    }

    public static SimulationPojo buildEmptySimulation(){

        SimulationPojo invalidSimulation = new SimulationPojo();

        invalidSimulation.setNome(null);
        invalidSimulation.setCpf(null);
        invalidSimulation.setEmail(null);
        invalidSimulation.setValor(null);
        invalidSimulation.setParcelas(null);
        invalidSimulation.setSeguro(null);

        return invalidSimulation;
    }

    public static SimulationPojo buildSimulationLessThanMinAmount(){
        Faker faker = new Faker();

        SimulationPojo simulation = SimulationDataFactory.buildValidSimulation();
        simulation.setValor(new BigDecimal(faker.number().numberBetween(1, MIN_AMOUNT - 1)));

        return simulation;
    }

    public static SimulationPojo buildSimulationExceedMaxAmount(){
        Faker faker = new Faker();

        SimulationPojo simulation = SimulationDataFactory.buildValidSimulation();
        simulation.setValor(new BigDecimal(faker.number().numberBetween(MAX_AMOUNT + 1, 99999)));

        return simulation;
    }

    public static SimulationPojo buildSimulationLessThanMinInstallments(){

        SimulationPojo simulation = SimulationDataFactory.buildValidSimulation();
        simulation.setParcelas(MIN_INSTALLMENTS - 1);

        return simulation;
    }

    public static SimulationPojo buildSimulationExceedInstallments(){
        Faker faker = new Faker();

        SimulationPojo simulation = SimulationDataFactory.buildValidSimulation();
        simulation.setValor(new BigDecimal(faker.number().numberBetween(MAX_INSTALLMENTS + 1, 99)));

        return simulation;
    }

    public static SimulationPojo updateSimulationData(SimulationPojo simulationToUpdate){

        Faker faker = new Faker();

        simulationToUpdate.setNome(faker.name().firstName());
        simulationToUpdate.setEmail(faker.internet().emailAddress());
        simulationToUpdate.setValor(BigDecimal.valueOf(faker.number().randomDouble(2,MIN_AMOUNT,MAX_AMOUNT)));
        simulationToUpdate.setParcelas(faker.number().numberBetween(MIN_INSTALLMENTS,MAX_INSTALLMENTS));
        simulationToUpdate.setSeguro(faker.bool().bool());

        return simulationToUpdate;
    }

    public static String notExistentCpf(){
        Faker faker = new Faker();

        String notExistentCpf = Long.toString(faker.number().randomNumber(11,true));
        return notExistentCpf;
    }

    private static final int MIN_AMOUNT = 1000;
    private static final int MAX_AMOUNT = 40000;
    private static final int MIN_INSTALLMENTS = 2;
    private static final int MAX_INSTALLMENTS = 48;

}
