package br.com.sicredi.simulacao.test.tests.simulations;

import br.com.sicredi.simulacao.test.factories.simulation.SimulationDataFactory;
import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import io.restassured.http.ContentType;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.ValidatableResponse;
import java.util.List;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.config.JsonConfig.jsonConfig;

public class SimulationBaseTest extends SimulationDataFactory{

    public static ValidatableResponse getAllSimulations(){

        return given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION)
                .then();
    }

    public static ValidatableResponse getSimulation(String cpf){
        return given()
                .contentType(ContentType.JSON)
                .config(config().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL)))
                .when()
                .get(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION + cpf)
                .then();
    }

    public static String getCpfFromNewSimulation(SimulationPojo simulationToGetCpf){
        return registerSimulation(simulationToGetCpf)
                .extract().path("cpf");
    }

    public static List<Integer> getAllSimulationsId(){

         List<Integer> idList = given()
                    .contentType(ContentType.JSON)
                .when()
                    .get(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION)
                .then()
                    .extract().path("id");

         return idList;
    }

    public static Integer getIdFromNewSimulation(SimulationPojo simulationToGetID){

        return registerSimulation(simulationToGetID)
                .extract().path("id");
    }

    public static SimulationPojo[] getAllApiSimulations(){

        return given()
                    .contentType(ContentType.JSON)
                .when()
                    .get(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION)
                .then()
                    .extract().as(SimulationPojo[].class);
    }

    public static ValidatableResponse registerSimulation(SimulationPojo simulationToRegister){

        return given()
                    .contentType(ContentType.JSON)
                    .config(config().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL)))
                    .body(simulationToRegister)
                .when()
                    .post(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION)
                .then();
    }

    public static ValidatableResponse deleteSimulation(Long id){
        return given()
                .contentType(ContentType.JSON)
                .config(config().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL)))
                .when()
                .delete(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION + id)
                .then();
    }

    public static void deleteAllSimulations() {

       List<Integer> idList = getAllSimulationsId();

       for(Integer id : idList) {
            deleteSimulation(Long.valueOf(id));
       }
    }

    public static ValidatableResponse updateSimulation(String existentCpf, SimulationPojo simulationToUpdate){

        return given()
                .contentType(ContentType.JSON)
                .config(config().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL)))
                .body(simulationToUpdate)
                .when()
                .put(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION + existentCpf)
                .then();
    }

    private static final String BASE_URL_SIMULATION = "http://localhost:8080";
    private static final String PATH_DEFAULT_SIMULATION = "/api/v1/simulacoes/";
}
