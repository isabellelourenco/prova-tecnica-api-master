package br.com.sicredi.simulacao.test.suite;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectPackages
        ({
                "br.com.sicredi.simulacao.test.tests.restrictions",
                "br.com.sicredi.simulacao.test.tests.simulations"

        })
public class RunAllSicrediApiTest {

}
