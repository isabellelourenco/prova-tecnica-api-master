package br.com.sicredi.simulacao.test.tests.simulations;

import br.com.sicredi.simulacao.test.factories.simulation.SimulationDataFactory;
import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;


public class SimulationContractTest extends SimulationBaseTest{

    @Test
    @DisplayName("When request a existing simulation by cpf, then should validate simulation schema")
    public void whenRequestExistingSimulation_ShouldValidateSimulationsSchema(){
        SimulationPojo simulationToRequest = SimulationDataFactory.buildValidSimulation();
        String cpfFromSimulation = getCpfFromNewSimulation(simulationToRequest);

        Integer idFromSimulation = getSimulation(cpfFromSimulation)
                .body(matchesJsonSchemaInClasspath("schemas/simulations_existing_schema.json"))
                .extract().path("id");

        deleteSimulation(Long.valueOf(idFromSimulation));
    }

    @Test
    @DisplayName("When request a non-existing simulation by cpf, then should validate simulation schema")
    public void whenRequestNonExistingSimulation_ShouldValidateSimulationsSchema(){

        String notExistentCpf = SimulationDataFactory.notExistentCpf();

        getSimulation(notExistentCpf)
                .body(matchesJsonSchemaInClasspath("schemas/simulations_not_existent_schema.json"));

    }

}
