package br.com.sicredi.simulacao.test.factories.restriction;

import com.github.javafaker.Faker;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RestrictionDataFactory {

    public static String cpfWithoutRestriction() {

        Faker faker = new Faker();

        return Long.toString(faker.number().randomNumber(11,true));

    }

    public static String cpfWithRestriction(){

        List<String> cpfWithRestriction = Arrays.asList(
                "97093236014",
                "60094146012",
                "84809766080",
                "62648716050",
                "26276298085",
                "01317496094",
                "55856777050",
                "19626829001",
                "24094592008",
                "58063164083"
        );

        String randomCpf = cpfWithRestriction.get(new Random().nextInt(cpfWithRestriction.size()));
        return randomCpf;
    }
}
