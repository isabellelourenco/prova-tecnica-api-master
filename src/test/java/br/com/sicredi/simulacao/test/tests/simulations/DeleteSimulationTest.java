package br.com.sicredi.simulacao.test.tests.simulations;

import br.com.sicredi.simulacao.test.factories.simulation.SimulationDataFactory;
import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.hamcrest.Matchers.equalTo;

public class DeleteSimulationTest extends SimulationBaseTest {

    SimulationPojo simulationToDelete = new SimulationPojo();

    @Test
    @DisplayName("When delete a simulation, then should be deleted with success")
    public void whenDeleteSimulation_ShouldBeDeletedWithSuccess(){

        simulationToDelete = SimulationDataFactory.buildValidSimulation();
        Integer simulationId = getIdFromNewSimulation(simulationToDelete);

        deleteSimulation(Long.valueOf(simulationId))
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    @DisplayName("When delete a simulation with non-existent id, then should return a not found error")
    public void whenDeleteSimulationWithNonExistentId_ShouldReturnErrorMessage(){

        simulationToDelete = SimulationDataFactory.buildValidSimulation();
        Integer simulationId = getIdFromNewSimulation(simulationToDelete);
        deleteSimulation(Long.valueOf(simulationId));

        deleteSimulation(Long.valueOf(simulationId))
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem",equalTo("Simulação não encontrada"));
    }

}
