package br.com.sicredi.simulacao.test.tests.restrictions;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.given;

public class RestrictionBaseTest {

    public ValidatableResponse requestRestriction(String cpf) {

        return given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_URL_SIMULATION + PATH_DEFAULT_SIMULATION + cpf)
                .then();
    }

    private static final String BASE_URL_SIMULATION = "http://localhost:8080";
    private static final String PATH_DEFAULT_SIMULATION = "/api/v1/restricoes/";
}
