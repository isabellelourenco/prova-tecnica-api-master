package br.com.sicredi.simulacao.test.tests.restrictions;

import br.com.sicredi.simulacao.test.factories.restriction.RestrictionDataFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class RestrictionContractTest extends RestrictionBaseTest {

    @Test
    @DisplayName("When request a cpf with restriction, then should validate restriction schema")
    public void shouldValidateRestrictionsSchema(){
        String cpfWithRestriction = RestrictionDataFactory.cpfWithRestriction();

        requestRestriction(cpfWithRestriction)
                .body(matchesJsonSchemaInClasspath("schemas/restrictions_schema.json"));
    }
}
