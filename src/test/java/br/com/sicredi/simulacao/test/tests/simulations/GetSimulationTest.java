package br.com.sicredi.simulacao.test.tests.simulations;

import br.com.sicredi.simulacao.test.factories.simulation.SimulationDataFactory;
import br.com.sicredi.simulacao.test.pojo.SimulationPojo;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GetSimulationTest extends SimulationBaseTest{

    @Test
    @DisplayName("When request simulations list with a simulation recorded, then should return a simulations list")
    public void whenRequestSimulationListWithRecordedSimulations_ShouldReturnSimulationsList(){
        SimulationPojo[] existingSimulations = getAllApiSimulations();

        SimulationPojo[] requestedSimulations = getAllSimulations()
                                .statusCode(HttpStatus.SC_OK)
                                .extract().as(SimulationPojo[].class);

        assertThat(existingSimulations,is(requestedSimulations));
    }

    @Test
    @DisplayName("When request a existing simulation by cpf, then should return a simulation")
    public void whenRequestExistingSimulation_ShouldReturnSimulation(){

        SimulationPojo simulationToRequest = SimulationDataFactory.buildValidSimulation();
        String cpfFromSimulation = getCpfFromNewSimulation(simulationToRequest);

        Integer idFromSimulation = getSimulation(cpfFromSimulation)
                .statusCode(HttpStatus.SC_OK)
                .body("nome", equalTo(simulationToRequest.getNome()))
                .body("cpf", equalTo(simulationToRequest.getCpf()))
                .body("email", equalTo(simulationToRequest.getEmail()))
                .body("valor", equalTo(simulationToRequest.getValor()))
                .body("parcelas", equalTo(simulationToRequest.getParcelas()))
                .body("seguro", equalTo(simulationToRequest.getSeguro()))
                .extract().path("id");

        deleteSimulation(Long.valueOf(idFromSimulation));

    }

    @Test
    @DisplayName("When request simulations list without record a simulation, then should return a empty list")
    public void whenRequestSimulationListWithoutRecordSimulation_ShouldReturnEmptyList(){

        deleteAllSimulations();

        getAllSimulations()
                .statusCode(HttpStatus.SC_NO_CONTENT)
                .body(equalTo("[]"));

    }

    @Test
    @DisplayName("When request a non-existing simulation by cpf, then should return a not found error")
    public void whenRequestNonExistingSimulation_ShouldReturnNotFound(){

        String notExistentCpf = SimulationDataFactory.notExistentCpf();

        getSimulation(notExistentCpf)
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem",equalTo("CPF " + notExistentCpf + " não encontrado"));

    }

}
