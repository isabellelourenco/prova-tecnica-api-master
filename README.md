
# Desafio Automação de API Sicredi

Teste desenvolvido com Java, Rest-Assured, Junit e Maven para testar uma API de simulação de crédito.

## Documentação da API

Para realização dos testes foram utilizados o [Swagger](http://localhost:8080/swagger-ui.html) da aplicação, que necessita do projeto estar rodando para ser possível visualizar. E também o arquivo PDF enviado pela empresa. 
## Dependências

| Lib | Descrição | 
|----------|--------------|
| Rest-Assured | Permite testar serviços REST em Java | 
| Junit | Executa os testes | 
| Jackson Core | Serializa e desserializa objetos Java | 
| Hamcrest | Para verificar os resultados em conjunto com o Junit | 
| Lombok | Faz uso de anotações para diminuir a verbosidade | 
| Java Faker | Gera dados aleatórios para serem utilizados nos testes |
| Maven | Gerencia dependências | 
| Allure | Gera um relatório HTML |

## Estrutura de diretórios

```
prova-tecnica-api-master-test/
 ├─ src/
 |   └─ test/
 |       ├─ java/
 |       |   ├─ factories/
 |       |   ├─ pojo/
 |       |   ├─ suite/
 |       |   └─ tests/
 |       └─ resources/
 |          └─ schemas/
 └─ pom.xml
```

- :file_folder: [java/](config): Dir que contém os arquivos de teste e os arquivos de apoio para realização dos testes
    - :file_folder: [factories/](factories): Dir com os arquivos que geram dados para apoiar os testes
    - :file_folder: [pojo/](pojo): Dir com os objetos java
    - :file_folder: [suite/](suite): Dir com arquivo que executa a suite de testes presentes no projeto
    - :file_folder: [tests/](tests): Dir com os testes e arquivos que auxiliam a automação
- :file_folder: [resources/](resources) Dir que contém os recursos para utilizar nos testes
    - :file_folder: [schemas/](schemas): Dir com os json schemas utilizados nos testes de contrato
- :page_with_curl: [pom.xml](pom.xml): Arquivo com as dependências do projeto.

## Executando os testes

Para executar todos os testes, execute na pasta raiz:

`mvn test`

Para gerar um relatório HTML e abrir o browser, execute:

`mvn allure:serve`

Para gerar um relatório HTML e gerar o relatório na pasta target/site/allure-maven-plugin, execute:

`mvn allure:report`


## Cenários de Teste

Os cenários de teste automatizados estão descritos abaixo, agrupados por Restrições e Simulações.

## Restrição 

#### Endpoint 

- GET <host>/api/v1/restricoes/{cpf}

|  | Cenário |  Resultado Esperado   | Resultado Atual | Causa | 
|-|---------|-----------|-----------|-----------|
|1| CPF sem restrição | Deve ser retornado o Status Code 204 | :heavy_check_mark: | - |
|2| CPF com restrição | Deve ser retornado o Status Code 200 e mensagem "O CPF 99999999999 possui restrição" |  :heavy_multiplication_x:  |   A mensagem retornada na response: "O CPF 99999999999 tem problema", é diferente da esperada   |

#### Validação do Json Schema

| | Cenário | Validação |  Resultado  |
|-----|----|-----|------|
| 3 |Response do CPF com restrição | Validar se o json da response contém a estrutura esperada presente na documentação | :heavy_check_mark: | 

## Simulação 

#### Endpoint

- POST <host>/api/v1/simulacoes

|| Cenário |  Resultado Esperado   | Resultado Atual | Causa | 
|-|---------|-----------|-----------|-----------|
|1| Cadastrar Simulação Válida | Quando cadastrar uma simulação com os dados válidos, então a simulação deve ser retornada com o status code 201 e os dados inseridos no response | :heavy_check_mark: | - |
|2| Cadastrar Simulação Inválida com campos vazios | Quando cadastrar uma simulação com os valores dos atributos vazios, então a simulação retorna o status code 400 e uma lista de erros  |  :heavy_multiplication_x:  |  Não está sendo retornada a mensagem de erro relacionada ao seguro, aparentemente o seguro está como "false" por default no banco de dados   |
|3| Cadastrar Simulação com CPF já existente | Quando cadastrar uma simulação com o CPF já existente, então a api retorna um status code 409 com a mensagem "CPF já existente" | :heavy_multiplication_x: |  O status code retornado (400) é divergente do esperado na documentação, assim como a mensagem ("CPF Duplicado") |
|4| Cadastrar Simulação com valor menor que o mínimo permitido | Quando cadastrar uma simulação com o valor menor do que o mínimo permitido, então deve ser retornado o status code 400 e a mensagem "Valor deve ser igual ou maior que R$ 1.000" |  :heavy_multiplication_x:  |   Esta sendo possível registrar a simulação   |
|5| Cadastrar Simulação com valor maior que o máximo permitido | Quando cadastrar uma simulação como valor maior do que o máximo permitido, então deve ser retornado o status code 400 e a mensagem "Valor deve ser menor ou igual a R$ 40.000" | :heavy_check_mark: | - |
|6| Cadastrar Simulação com a parcela menor que o mínimo permitido | Quando cadastrar uma simulação com a parcela menor do que o mínimo permitido, então deve ser retornado o status code 400 e a mensagem "Parcelas deve ser igual ou maior que 2" |  :heavy_check_mark:  |  -  |
|7| Cadastrar Simulação com a parcela maior que o máximo permitido | Quando cadastrar uma simulação com a parcela maior do que o máximo permitido, então deve ser retornado o status code 400 e a mensagem "Parcelas deve ser menor ou igual a 48" |  :heavy_multiplication_x:  |  Esta sendo possível registrar a simulação    |


- PUT <host>/api/v1/simulacoes/{cpf}

|| Cenário |  Resultado Esperado   | Resultado Atual | Causa | 
|-|---------|-----------|-----------|-----------|
|8| Atualizar Simulação com Dados Válidos | Quando atualizar uma simulação com dados válidos, então a simulação deve ser atualizada retornando o status code 200 e os dados atualizados inseridos no response | :heavy_multiplication_x: | O valor não está sendo atualizado |
|9| Atualizar Simulação informando CPF que não existe | Quando atualizar um cpf que não possui uma simulação, então é retornado um status code 404 e a mensagem "CPF não encontrado"  |  :heavy_multiplication_x:  |  A mensagem retornada ("CPF 999999999 não encontrado") é divergente da presente na documentação  |

- GET <host>/api/v1/simulacoes

|| Cenário |  Resultado Esperado   | Resultado Atual | Causa | 
|-|---------|-----------|-----------|-----------|
|10|  Consultar todas simulações cadastradas | Quando consultar todas as simulações cadastradas, então deve ser retornado o status code 200 e uma lista com as simulações cadastradas | :heavy_check_mark: | - |
|11|  Consultar todas as simulações sem simulações pré cadastradas | Quando consultar todas as simulações sem possuir simulações cadastradas, então é retornado um status code 204 e a lista vazia  |  :heavy_multiplication_x:  |  O status code retornado (200) é divergente do esperado na documentação  |

- GET <host>/api/v1/simulacoes/{cpf}

|| Cenário |  Resultado Esperado   | Resultado Atual | Causa | 
|-|---------|-----------|-----------|-----------|
|12|  Consultar uma simulação cadastrada | Quando consultar uma simulação cadastrada pelo CPF, então deve ser retornado o status code 200 e a simulação cadastrada no cpf | :heavy_check_mark: | - |
|13|  Consultar uma simulação não cadastrada | Quando consultar uma simulação por um CPF não registrado, então é retornado um status code 404 e a mensagem "CPF 99999999999 não encontrado" |  :heavy_check_mark:  |  -  |

- DELETE <host>/api/v1/simulacoes/{id}

|| Cenário |  Resultado Esperado   | Resultado Atual | Causa | 
|-|---------|-----------|-----------|-----------|
|14|  Deletar uma simulação cadastrada | Quando deletar uma simulação cadastrada, então deve ser retornado o status code 204 e a simulação deve ser deletada com sucesso | :heavy_multiplication_x: | O status code retornado (200) é divergente do esperado na documentação |
|15|  Deletar uma simulação não cadastrada | Quando deletar uma simulação não cadastrada, então é retornado um status code 404 e a mensagem "Simulação não encontrada" |  :heavy_multiplication_x:  |  O status code retornado (200) é divergente do esperado na documentação, assim como a mensagem "OK" |

#### Validação do Json Schema

|| Cenário |  Validação  | Resultado | 
|-|---------|-----------|-----------|
|16| Response de uma simulação existente | Validar se o json da response contém a estrutura esperada presente na documentação | :heavy_check_mark: | 
|17| Response de uma simulação não existente | Validar se o json da response contém a estrutura esperada presente na documentação | :heavy_check_mark: | 
